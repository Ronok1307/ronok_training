package task1;

import com.google.common.base.*;

public class ConvertBackAndFront extends Converter<BackEnd,FrontEnd>{

  @Override
  protected BackEnd doBackward(FrontEnd arg0) {
    BackEnd a= new BackEnd();
    a.age=arg0.age;
    a.name=arg0.name;
    return a;
  }

  @Override
  protected FrontEnd doForward(BackEnd arg0) {
    FrontEnd a = new FrontEnd();
    a.age=arg0.age;
    a.name=arg0.name;
    return a;
  }
  
}
