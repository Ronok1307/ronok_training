package task1;

public class task1 {
  public static void main(String[] args)
  {
    BackEnd a = new BackEnd();
    a.age=31;
    a.name="XYZ";
    a.publicKey=123456;
    ConvertBackAndFront b= new ConvertBackAndFront();
    FrontEnd c = b.convert(a);
    System.out.println(c.name + " " + c.age);
    BackEnd d=b.reverse().convert(c);
    System.out.println(d.name + " " + d.age + " " + d.publicKey);
  }
}