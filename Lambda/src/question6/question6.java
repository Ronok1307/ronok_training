package question6;

import java.util.*;
import java.util.stream.*;

public class question6 {
  public static void main(String[] args)
  {
    List <Engineer> list = new ArrayList <Engineer>();
    list.add(new Engineer("A","CSE",22));
    list.add(new Engineer("B","EEE",22));
    list.add(new Engineer("C","CSE",36));
    Stream <Engineer> asd= list.stream().filter(p -> p.subject=="CSE" && p.age<35);
    asd.forEach(p -> p.printDetails());
  }
}
