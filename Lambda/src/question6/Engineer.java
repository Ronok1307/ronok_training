package question6;

public class Engineer {
  int age;
  String name;
  String subject;
  
  public Engineer(String a, String b, int c)
  {
   name=a;
   subject=b;
   age=c;
  }
  
  public void printDetails()
  {
    System.out.println(name + " " +  subject + " " + age);
  }
}
