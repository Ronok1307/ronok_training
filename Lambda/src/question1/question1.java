package question1;

import java.util.*;


/*You will be given an array of integers, find the sum of these integer using lambda
  expression, you should not use conventional for / while loops for array traversing.
 */

public class question1 
{
  public static void main(String[] args)
  {
    List <Integer> list = new ArrayList<Integer>();
    list.add(1);
    list.add(2);
    list.add(3);
    //Create a stream from the list and map these to it's integer values and add them
    System.out.println(list.stream().mapToInt(i-> i).sum());
  }
}
