package question3;

import java.util.function.*;

// Write a lambda function that take two integer parameters and  return their gcd.

public class question3 {
  public static void main(String[] args)
  {
    Integer a,b;
    a=123;
    b=63;
    /*BinaryOperator is a built-in interface that takes two parameters of same type
     * returns a value of the same type. The only function of this interface is apply
     */
    BinaryOperator<Integer> gcd = (d,e) -> {
      while(e!=0)
      {
        d=d%e;
        Integer f=d;
        d=e;
        e=f;
      }
      return d;
    };
    System.out.println(gcd.apply(a, b));
  }
}
